<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $fillable = ['picture', 'title', 'description', 'user', 'author'];
    protected $table = 'ideas';
}
