<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\Album as AlbumResource;
use App\Album;

class AlbumController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return new AlbumResource(Album::all());
    }

    /*
        SHOW
    */
    public function show($id) : LocationResource
    {
        return new AlbumResource(Album::find($id));
    }

    /*
        UPDATE
    */
    public function update(Request $request, Album $album)
    {
        $album->update($request->all());

        return response()->json($album, 200);
    }

    /*
        STORE
    */
    public function store(Request $request)
    {
        // file validation
        $validator = $request->validate(
            [
                'filename' => 'required|mimes:jpeg,png,jpg,bmp|max:2048'
            ]);

        // if validation success
        if($file = $request->file('filename')) {

        $name = time() . time() . '.' . $file->getClientOriginalExtension();
        
        $target_path = public_path('/uploads/');
        
            if($file->move($target_path, $name)) {
               
                // save file name in the database
                $file = Album::create($request->all());
            }
        }
        return response()->json($target_path, 201);
    }

    /*
        DELETE
    */
    public function delete(Album $album)
    {
        $album->delete();

        return response()->json(null, 204);
    }
}
