<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FileUploadController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function store(Request $request)
    {
        // file validation
        $validator = Validator::make($request->all(),
            [
                'filename' => 'required|mimes:jpeg,png,jpg,bmp|max:2048'
            ]);

        // if validation fails
        if($validator->fails()) {
            return back()->withErrors($validator->errors());
        }

        // if validation success
        if($file = $request->file('filename')) {

        $name = time() . time() . '.' . $file->getClientOriginalExtension();
        
        $target_path = public_path('/uploads/');
        
            if($file->move($target_path, $name)) {
               
                // save file name in the database
                $file = File::create(['filename' => $name]);
            
                return back()->with("success", "File uploaded successfully");
            }
        }
    }
}
