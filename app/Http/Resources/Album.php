<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\User as UserResource;

class Album extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'picture' => $this->picture,
            'title' => $this->title,
            'created_at' => $this->created_at,
            'description' => $this->description,
            'author' => $this->author,
            'user' => new UserResource($this->user),
        ];
    }
}
