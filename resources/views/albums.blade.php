@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                <div class="container mt-5">
                    <form action="{{ route('create_album') }}" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 m-auto">
                                <div class="card shadow">
                                    <div class="card-header bg-info text-white">
                                        <div class="card-title ">
                                            <h4> Laravel 6 File Upload </h4>
                                        </div>                       
                                    </div>

                                    <div class="card-body">

                                    <!-- print success message after file upload  -->
                                            @if(Session::has('success'))
                                                <div class="alert alert-success">
                                                    {{ Session::get('success') }}
                                                    @php
                                                        Session::forget('success');
                                                    @endphp
                                                </div>
                                            @endif

                                            <div class="form-group" {{ $errors->has('filename') ? 'has-error' : '' }}>
                                                <label for="picture"></label>
                                                    <input type="file" name="picture" id="picture" class="form-control">
                                                    <span class="text-danger"> {{ $errors->first('picture') }}</span>
                                            </div>
                                            <div class="form-group">
                                            <input type="text" name="title" id="title" class="form-control">
                                            <input type="text" name="description" id="description" class="form-control">
                                            </div>
                                    </div>

                                    <div class="card-footer">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success btn-md"> Upload </button>
                                        </div>
                                        {{ csrf_field() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
    
                    <div class="alert alert-success" role="alert">
                        <p>Album</p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
